/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"


//==============================================================================
MainComponent::MainComponent()
{
    setSize (500, 400);
    textButton1.setButtonText ("Push My Button");
    addAndMakeVisible(textButton1);
    
    slider1.setSliderStyle (Slider::Rotary);
    addAndMakeVisible (slider1);
    
}

MainComponent::~MainComponent()
{

}

void MainComponent::resized()
{
    textButton1.setBounds (10, 10, getWidth() - 20, 40);
    slider1.setBounds (10, 50, getWidth() - 20, 40);
    
    DBG (getWidth());
    DBG (getHeight());

}